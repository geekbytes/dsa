/*
 * Copyright (c) 2020.
 * Manu T S [http://geekbytes.io]
 */

import sbt.Resolver

object Globals {

  val RESOLVERS = Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )

  val SCALA_VERSION = "2.12.8"

}
