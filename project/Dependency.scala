/*
 * Copyright (c) 2020.
 * Manu T S [http://geekbytes.io]
 */

import sbt._

//noinspection TypeAnnotation
trait Dependency {

  def * : Seq[ModuleID]

  def test: Seq[ModuleID] = *.map(_ % Test)

}

object Dependency {
  implicit def dependencyToSeq(dependency: Dependency): Seq[sbt.ModuleID] = dependency.*
}

object Dependencies {


  object Config extends Dependency {

    val pureConfig = "com.github.pureconfig" %% "pureconfig" % "0.12.2"

    val scalaConfig = "com.github.andr83" %% "scalaconfig" % "0.6"

    val typeSafeConfig = "com.typesafe" % "config" % "1.4.0"

    def * : Seq[ModuleID] = Seq(typeSafeConfig, scalaConfig)

  }

  object Tests extends Dependency {

    val scalaTest = "org.scalatest" %% "scalatest" % "3.1.0"

    val mockito = "org.mockito" %% "mockito-scala" % "1.11.2"

    val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.1"

    val randomDataGen = "com.danielasfregola" %% "random-data-generator" % "2.4"

    def * : Seq[ModuleID] = Seq(scalaTest, mockito, randomDataGen).map(_ % Test)

  }

  object Logging extends Dependency {

    val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"

    def *  = Seq("com.typesafe.scala-logging" %% "scala-logging" % "3.9.2")

  }

  object Lens extends Dependency {
    def *  = Seq("com.softwaremill.quicklens" %% "quicklens" % "1.4.12")
  }

  object ShapeLess extends Dependency {
    def *  = Seq("com.chuusai" %% "shapeless" % "2.3.3")
  }

  object common extends Dependency {
    def * : Seq[sbt.ModuleID] = Lens ++ Logging ++ Tests
  }

  object app extends Dependency {
    def * : Seq[sbt.ModuleID] = Config :+ Config.pureConfig :+ Logging.logback
  }

}
