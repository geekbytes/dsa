lazy val service = {
  project
    .in(file("TestApp"))
    .settings(Common.appSettings)
    .settings(name := "test")
}