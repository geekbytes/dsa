package practice.array

object PartitionToMinimumAbsoluteDifeerence extends App {

  //https://www.geeksforgeeks.org/partition-a-set-into-two-subsets-such-that-the-difference-of-subset-sums-is-minimum/
  // O(2^n) complexity
  def findPartitionRec(array: Array[Int]): (Int, List[Int]) = {

    val n = array.length

    val sum = {
      var sum = 0;
      for {
        i <- 0 until n
      } {

        sum = sum + array(i)

      }
      sum
    }

    import collection.immutable.List

    def findMinSum(curentIndex: Int, calculatedSum: Int, indexes: collection.immutable.List[Int]): (Int, List[Int]) = {

      if (curentIndex < 0) {
        math.abs((sum - calculatedSum) - calculatedSum) -> indexes
      } else {

        val item = array(curentIndex)

        val (includedSum, includedIndexes) = findMinSum(curentIndex - 1, calculatedSum + item, curentIndex :: indexes)
        val (excludedSum, excludedIndexes) = findMinSum(curentIndex - 1, calculatedSum, indexes)

        if (includedSum < excludedSum) {
          (includedSum, includedIndexes)
        } else {
          (excludedSum, excludedIndexes)
        }

      }

    }

    findMinSum(n - 1, 0, Nil)

  }

  println(findPartitionRec(Array(1, 5, 11, 6)))
  println(findPartitionRec(Array(3, 1, 4, 2, 2, 1 )))

}
