package practice.array

object PartitionToEqualSum extends App {

  import collection.mutable._
  import scala.collection.mutable

  //https://www.geeksforgeeks.org/partition-problem-dp-18/
  // O(2^n) complexity
  def findPartitionRec(array: Array[Int]) = {

    def isSubSetSum(n: Int, halfSum: Int): Boolean = {

      if (halfSum == 0) {
        true
      } else if (n == 0 && halfSum != 0) {
        false
      } else {

        //last element is > sum (sum => totalSum/2) then its not part of the sub array with half its sum
        if (array(n - 1) > halfSum) {
          isSubSetSum(n - 1, halfSum)
        } else {
          /*repeat check for next sub array excluding last element in sum*/
          val isSubSetExcludingLast = isSubSetSum(n - 1, halfSum)

          /*repeat check for next sub array including last element in sum */
          lazy val isSubSetIncludingLast = isSubSetSum(n - 1, halfSum - array(n - 1))

          isSubSetExcludingLast || isSubSetIncludingLast
        }

      }

    }

    val n = array.length

    val sum = {
      var sum = 0;
      for {
        i <- 0 until n
      } {

        sum = sum + array(i)

      }
      sum
    }

    if (sum % 2 == 0) {
      isSubSetSum(n, sum / 2)
    } else false /*if sum is odd there will be no sub array*/

  }

  println(findPartitionRec(Array(1, 5, 11, 5)))

}
