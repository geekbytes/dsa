package practice

object findAnagrams extends App {

  //https://leetcode.com/problems/find-all-anagrams-in-a-string

  import scala.collection.mutable._

  def findAnagrams(s: String, p: String): List[Int] = {

    val pMap = CharMap.parse(p)

    println(pMap)

    val result: ArrayBuffer[Int] = ArrayBuffer[Int]()

    if (pMap.length > s.length) {
      Nil
    } else {

      for {
        i <- 0 to (s.length - pMap.length)
      } {
        if (pMap.checkAnagram(s, i)) {
          result += i
        }
      }

      result.toList

    }

  }

  trait CharMap {

    def length: Int

    def add(char: Char): Unit

    def checkAnagram(s: String, startIndex: Int): Boolean = {
      val sMap = CharMap.empty

      for {
        i <- 0 until length
      } {
        val index = startIndex + i
        val char = s.charAt(index)

        if (sMap.getCount(char) < getCount(char)) {
          sMap.add(char)
        } else {
          return false
        }

      }

      true

    }

    def getCount(char: Char): Int

  }

  object CharMap {

    def parse(s: String): ArrayCharMap = {
      val map = ArrayCharMap()
      s.foreach(map.add)
      map

    }

    def empty: CharMap = ArrayCharMap()

    def getCharIndex(c: Char): Int = c - 'a'

    case class ArrayCharMap(array: ArrayBuffer[Int] = ArrayBuffer.fill(26)(0), var length: Int = 0) extends CharMap {

      def add(char: Char): Unit = {

        val index = getCharIndex(char)

        length += 1

        array.update(index, array(index) + 1)

      }

      def getCount(char: Char): Int = {

        val index = getCharIndex(char)

        array(index)

      }

      override def toString: String = s"${array}/$length"

    }
  }

  println(findAnagrams("cbaebabacd","abc"))

}
