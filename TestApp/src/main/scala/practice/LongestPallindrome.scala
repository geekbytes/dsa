package practice



object LongestPallindrome extends App {

  //https://leetcode.com/problems/longest-palindromic-substring/solution/

  import scala.collection.mutable

  def longestPalindrome(s: String): String = {

    val length = s.length

    var p = ""

    def check(sb: mutable.StringBuilder, l: Int, r: Int): Unit = {

      if (l >= 0 && r < length) {
        val lc = s.charAt(l)
        val rc = s.charAt(r)

        if (lc == rc) {
          check(sb.append(rc).insert(0, lc), l - 1, r + 1)
          return
        }

      }

      if (sb.length > p.length) {
        p = sb.toString()
      }

    }

    for {
      i <- s.indices
    } {

      check(new mutable.StringBuilder(), i, i + 1)
      check(new mutable.StringBuilder(s.charAt(i).toString), i - 1, i + 1)

    }

    p

  }

}
