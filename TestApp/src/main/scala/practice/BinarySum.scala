package practice

import scala.collection.mutable
import scala.collection.mutable._

object BinarySum extends App {

  //https://leetcode.com/problems/add-binary/

  val lookUp = Map(
    Operators('0', '0', '0') -> Result('0', '0'),
    Operators('0', '0', '1') -> Result('1', '0'),
    Operators('0', '1', '0') -> Result('1', '0'),
    Operators('0', '1', '1') -> Result('0', '1'),
    Operators('1', '0', '0') -> Result('1', '0'),
    Operators('1', '0', '1') -> Result('0', '1'),
    Operators('1', '1', '0') -> Result('0', '1'),
    Operators('1', '1', '1') -> Result('1', '1')
  )

  def addBinary(a: String, b: String): String = {

    val resultLength = math.max(a.length, b.length)

    val result = new mutable.StringBuilder("")


    var l = a.length - 1
    var r = b.length - 1
    var carry = '0'
    var i = resultLength

    while (l >= 0 || r >= 0 || carry == '1') {

      val operators = Operators(carry, {
        if (l >= 0) {
          a(l)
        } else '0'
      }, {
        if (r >= 0) {
          b(r)
        } else '0'
      })
      val Result(sum, newCarry) = lookUp(operators)

      result.insert(0,sum)

      carry = newCarry

      i = i - 1
      l = l - 1
      r = r - 1
    }



    result.mkString("")

  }

  case class Result(sum: Char, carry: Char)

  case class Operators(carry: Char, left: Char, right: Char)

  println(addBinary("0", "0"))

}
