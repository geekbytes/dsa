package practice

object Heap extends App {

  import collection.mutable._
  import scala.collection.mutable

  case class PriorityQueue private (array: ArrayBuffer[Int]) {

    def insert(item: Int): Unit = {
      array += item
      sort.heapifyArray(array)
    }

    def remove(): Unit = {
      sort.swap(array, 0, array.length - 1)
      array.remove(array.length - 1, 1)
      sort.heapifyArray(array)
    }

  }

  object PriorityQueue {

    def apply(array: ArrayBuffer[Int]): PriorityQueue = {

      sort.heapifyArray(array)

      new PriorityQueue(array)

    }
  }

}
