package practice

object MatchParanthesis extends App {

  import scala.collection.mutable

  //https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/

  def minRemoveToMakeValid2Pass(s: String): String = {

    val stringBuilder = new mutable.StringBuilder()

    var open = 0
    var balance = 0

    s.foreach({
      case c @ '(' => {
        open += 1
        balance += 1
        stringBuilder.append(c)
      }
      case c @ ')' if balance > 0 => {
        balance -= 1
        stringBuilder.append(c)
      }
      case ')' => {}
      case c => {
        stringBuilder.append(c)
      }
    })

    var unmatchedOpen = open - balance

    val result = new mutable.StringBuilder()
    stringBuilder.foreach({
      case c @ '(' if unmatchedOpen > 0 => {
        unmatchedOpen -= 1
        result.append(c)
      }
      case '(' => {}
      case c   => result.append(c)
    })

    result.toString()

  }

  def minRemoveToMakeValid(s: String): String = {

    val stringBuilder = new mutable.StringBuilder()

    val stack = new mutable.Stack[Int]()

    val indexesToremove = mutable.Set.empty[Int]

    s.zipWithIndex.foreach({
      case ('(', i) => {
        stack.push(i)
      }
      case (c @ ')', i) if stack.isEmpty => {
        indexesToremove.add(i)
      }
      case (c @ ')', i) => {
        stack.pop()
      }
      case _ => {}
    })

    while (stack.nonEmpty) {
      indexesToremove.add(stack.pop())
    }

    s.zipWithIndex.foreach({
      case (c, i) if !indexesToremove.contains(i) => {
        stringBuilder.append(c)
      }
      case _ => {}
    })

    stringBuilder.toString()

  }

}
