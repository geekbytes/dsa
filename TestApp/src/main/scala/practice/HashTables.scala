package practice

object HashTables extends App {

  import scala.collection.mutable

  val hasTable = mutable.HashMap.empty[Int, String]

  hasTable += (2 -> "two")
  hasTable += (4 -> "four")
  hasTable += (3 -> "three")

  println(hasTable)

  hasTable.remove(2)

  println(hasTable)

}
