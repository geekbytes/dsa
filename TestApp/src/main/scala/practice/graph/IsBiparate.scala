package practice.graph.biparate

import Graph._
import Biparate.Color

import scala.collection.mutable

case class Graph(edges: Array[Edges]) {

  import scala.collection.mutable._

  lazy val length: Int = edges.length

  def getEdgesForNode(i: Node): Edges = {
    edges(i)
  }

  lazy val isBiparate: Boolean = checkBiparate()

  private def checkBiparate(): Boolean = {
    val colors = Array.fill[Color](length)(Color.UnColored)

    val stack = new mutable.Stack[Node]

    for {
      i <- edges.indices
    } {
      val nodeColor = colors(i)
      if (nodeColor == Color.UnColored) {
        colors.update(i, nodeColor.next)
        stack.push(i)
        while (stack.nonEmpty) {
          val node = stack.pop()
          for {
            j <- getEdgesForNode(node)
          } {
            val edgeColor = colors(j)
            if (edgeColor == Color.UnColored) {
              colors.update(j, nodeColor.next)
              stack.push(j)
            } else if (nodeColor == edgeColor) {
              return false
            }
          }
        }
      }
    }

    true

  }

}

object Graph {
  type Node = Int
  type Edges = Array[Node]
}

object Biparate {

  sealed trait Color {
    def next: Color
  }

  object Color {
    case object UnColored extends Color {
      def next: Color = Color.Blue
    }
    case object Red extends Color {
      def next: Color = Color.Blue
    }
    case object Blue extends Color {
      def next: Color = Color.Red
    }
  }

}
