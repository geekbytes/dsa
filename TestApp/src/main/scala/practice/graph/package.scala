package practice



package object graph {

  import scala.util.{Failure, Success, Try}
  import scala.collection.mutable

  case class Graph[K, A](nodes: mutable.Map[K, GraphNode[K, A]] = mutable.Map.empty[K, GraphNode[K, A]]) {

    def addNode(node: GraphNode[K, A]): GraphNode[K, A] = {
      nodes.update(node.key, node)
      node
    }

    def link(source: K, target: K): Option[(GraphNode[K, A], GraphNode[K, A])] = {
      (getNode(source), getNode(target)) match {
        case (Some(source), Some(target)) => {
          source.addEdge(target)
          Some(source -> target)
        }
        case _ => {
          println("unable to add edge")
          None
        }
      }
    }

    def getNode(key: K): Option[GraphNode[K, A]] = nodes.get(key)

    def dfs(root: GraphNode[K, A])(result: GraphNode[K,A] => Unit): Unit = {

      val visited = mutable.Set.empty[K]
      val stack = new mutable.Stack[GraphNode[K, A]]()

      stack.push(root)

      while (stack.nonEmpty) {
        val item = stack.pop()
        visited.add(item.key)

        //operation

        item.edges.foreach(edge => {
          if(!visited.contains(edge.key)) {
            stack.push(edge)
          }
        })

        //operation

      }

    }

    def dfsWithCycle(root: GraphNode[K, A], reverse: Boolean = false)(result : GraphNode[K,A] => Unit): Try[Unit] = {

      val visited = mutable.Set.empty[K]
      val inStack = mutable.Set.empty[K]
      val stack = new mutable.Stack[GraphNode[K, A]]()

      stack.push(root)

      dfsInternal(visited, inStack, stack, result)(reverse)

    }

    def dfsAllNodesWithCycle(reverse: Boolean = false)(result: GraphNode[K,A] => Unit): Try[Unit] = {

      val visited = mutable.Set.empty[K]
      val inStack = mutable.Set.empty[K]
      val stack = new mutable.Stack[GraphNode[K, A]]()

      nodes.foreach({
        case (k, node) if !visited.contains(k) => {
          stack.push(node)
          dfsInternal(visited, inStack, stack, result)(reverse) match {
            case Success(_) => {
              //no cycle continue
            }
            case error => return error
          }
        }
        case _ => {}
      })

      Success(())

    }

    private def dfsInternal(visited: mutable.Set[K],
                            inStack: mutable.Set[K],
                            stack: mutable.Stack[GraphNode[K, A]],
                            result: GraphNode[K,A] => Unit)(reverse: Boolean = false): Try[Unit] = {

      while (stack.nonEmpty) {

        val item = stack.top
        val key = item.key

        if (visited.contains(key)) {
          inStack -= key
          stack.pop()
          if (reverse) {
            result(item)
          }
        } else {
          visited.add(key)
          inStack.add(key)
          if (!reverse) {
            result(item)
          }
        }

        item.edges.foreach(adj => {

          val key = adj.key

          if (!visited.contains(key)) {
            stack.push(adj)
          } else if (inStack.contains(key)) {
            return Failure(Graph.Cycle(adj))
          }

        })

      }

      Success(())

    }

    def hasCycle: Boolean = {

      val visited = mutable.Set.empty[K]
      val inStack = mutable.Set.empty[K]
      val stack = new mutable.Stack[GraphNode[K, A]]()

      nodes.foreach({
        case (k, node) if !visited.contains(k) => {
          stack.push(node)
          while (stack.nonEmpty) {
            val item = stack.top
            val key = item.key
            if (visited.contains(key)) {
              inStack -= key
              stack.pop()
            } else {
              visited.add(key)
              inStack.add(key)
            }

            item.edges.foreach(n => {
              val key = n.key
              if (!visited.contains(key)) {
                stack.push(n)
              } else if (inStack.contains(key)) {
                return true
              }
            })
          }
        }
        case _ => {}
      })
      false

    }

  }

  case class GraphNode[K, A](value: A, edges: mutable.Set[GraphNode[K, A]] = mutable.Set.empty[GraphNode[K, A]])(
      getKey: A => K
  ) {

    def key: K = getKey(value)

    def addEdge(target: GraphNode[K, A]): Unit = {
      edges += target
    }
  }

  object GraphNode {
    def int(i: Int): GraphNode[Int, Int] = GraphNode(i)(identity)
  }

  object Graph {

    def empty[K, A]: Graph[K, A] = Graph[K, A]()

    case class Cycle[K, A](node: GraphNode[K, A]) extends Throwable

  }

}
