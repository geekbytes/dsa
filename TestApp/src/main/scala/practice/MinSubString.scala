package practice

object MinSubString extends App {

  def minWindow(s: String, t: String): String = {

    import scala.collection.mutable._

    val tMap = Map.empty[Char,Int]
    var letters = 0


    t.foreach(c => {
      tMap.update(c, tMap.getOrElse(c, {
        letters += 1
        0
      }) + 1)
    })

    var last = Option.empty[Char]

    var r = 0
    val sMap = Map.empty[Char,Int]
    var count = 0
    var result = ("",Int.MaxValue)

    while(r < s.length()){

      val c = s.charAt(r)

      sMap.update(c, sMap.getOrElse(c, 0) + 1)

      if(tMap.getOrElse(c,0) == sMap(c)) {
        count += 1
      }


      if(count == letters && last.fold(true)(_ == c)) {

        var l = r

        val wMap = tMap.clone()

        while(wMap.nonEmpty) {

          val c = s.charAt(l)

          if(wMap.contains(c)) {
            val k = wMap(c)
            if(k == 1) {
              wMap -= c
              if(wMap.isEmpty){
                last = Some(c)
              }
            } else {
              wMap.update(c,k-1)
            }
          }

          l -=1
        }

        if(result._2 > r-l) {
          result = (s.substring(l+1,r+1),r-l)
        }

      }

      r +=1

    }

    result._1






  }


  println(minWindow("ADOBECODEBANC","ABC"))

}
