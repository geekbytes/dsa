package practice.reference
import practice.sort.swap
import java.util.concurrent.ThreadLocalRandom

object sort {

  object Solution {

    def solution(a: Array[Int]): Int = {
      // write your code in Scala 2.12

      val set = a.foldLeft(Set.empty[Int])({
        case (set, i) if i >= 0 => set + i
        case (set, _)           => set
      })

      (1 to a.length).find(i => !set.contains(i)).getOrElse(1)

    }
  }

  object HeapSort extends App {
    import scala.collection.mutable._

    def sort(array: ArrayBuffer[Int]): ArrayBuffer[Int] = {
      val n = array.length
      heapifyArray(array)
      for {
        i <- (n - 1) to 0 by -1
      } {
        swap(array, i, 0)
        heapify(array, i, 0)
      }
      array
    }

    def heapifyArray(array: ArrayBuffer[Int]): Unit = {
      val length = array.length
      for {
        i <- (length / 2 - 1) to 0 by -1
      } {
        heapify(array, length, i)
      }
    }

    @scala.annotation.tailrec
    def heapify(array: ArrayBuffer[Int], n: Int, i: Int): Unit = {
      var indexOfLargest = i
      val l = 2 * i + 1
      val r = 2 * i + 2
      if (l < n && array(l) > array(indexOfLargest)) {
        indexOfLargest = l
      }
      if (r < n && array(r) > array(indexOfLargest)) {
        indexOfLargest = r
      }
      if (indexOfLargest != i) {
        swap(array, i, indexOfLargest)
        heapify(array, n, indexOfLargest)
      }
    }
    println(sort(ArrayBuffer(1, 50, 6, 9, 10, 12)))

  }

  object QuickSort extends App {
    type Index = Int

    def partition[A: Ordering](array: Array[A])(l: Index, h: Index): Index = {
      val rand = ThreadLocalRandom.current().nextInt(l, h)
      val pivot = array(rand)
      var i = l
      for {
        j <- l to h
      } {
        if (Ordering[A].compare(array(j), pivot) < 0) {
          swap(array, i, j)
          i += 1
        }
      }
      swap(array, i, rand)
      i
    }

    def quickSort[A: Ordering](array: Array[A])(l: Index, h: Index): Unit = {
      if (l < h) {
        val p = partition(array)(l, h)
        quickSort(array)(l, p - 1)
        quickSort(array)(p + 1, h)
      }
    }

    def apply[A: Ordering](array: Array[A]): Unit = {
      quickSort(array)(0, array.length - 1)
    }
  }
}
