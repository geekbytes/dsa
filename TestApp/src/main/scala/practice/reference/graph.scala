package practice.reference

import practice.graph.GraphNode
import practice.sort.swap

import java.util.concurrent.ThreadLocalRandom
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, Set}

object graph {

  object DFSBFSRec {
    val result = ArrayBuffer.empty[Int]

    val visited: Set[Int] = Set.empty
    val visiting: Set[Int] = Set.empty

    def dfs(node: Int): Unit = {
      if (visiting.contains(node)) {
        throw new Exception("cyclic dependency")
      }
      visiting.add(node)
      if (!visited.contains(node)) {
        val dependencies = projectDependencies.getOrElse(node, Array.emptyIntArray)
        dependencies.foreach(dfs)
        result.append(node)
        visited.add(node)
      }
      visiting.remove(node)
    }

    def bfs(node: Int):Unit = {
      if (visiting.contains(node)) {
        throw new Exception("cyclic dependency")
      }
      visiting.add(node)
      if (!visited.contains(node)) {
        result.append(node)
        val dependencies = projectDependencies.getOrElse(node, Array.emptyIntArray)
        dependencies.foreach(bfs)
        visited.add(node)
      }
      visiting.remove(node)
    }
  }

  object DFS_BFS_NonRec extends App {

    def dfs[A](node: GraphNode[String,  A])(key: A => String, traverse: A => Unit): Unit = {
      val visited = mutable.Set[String]()
      val stack = new mutable.Stack[GraphNode[String, A]]()
      stack.push(node)
      while (stack.nonEmpty) {
        val item = stack.pop()
        if (!visited.contains(key(item.value))) {
          traverse(item.value)
          visited.update(key(item.value), included = true)
          item.edges.filter(i => !visited.contains((i.key))).foreach(stack.push)
        }
      }
    }
    def bfs[A](node: GraphNode[String, A])(key: A => String, traverse: A => Unit): Unit = {
      val visited = mutable.Set[String]()
      val queue = new mutable.Queue[GraphNode[String, A]]()
      queue.enqueue(node)
      while (queue.nonEmpty) {
        val item = queue.dequeue()
        if (!visited.contains(key(item.value))) {
          traverse(item.value)
          visited.update(key(item.value), included = true)
          item.edges.filter(i => !visited.contains(key(i.value))).foreach(item => queue.enqueue(item))
        }
      }
    }
  }

  object graph {
    def hasCycle: Boolean = {

      val visited = mutable.Set.empty[K]
      val inStack = mutable.Set.empty[K]
      val stack = new mutable.Stack[GraphNode[K, A]]()
      nodes.foreach({
        case (k, node) if !visited.contains(k) => {
          stack.push(node)
          while (stack.nonEmpty) {
            val item = stack.top
            val key = item.key
            if (visited.contains(key)) {
              inStack -= key
              stack.pop()
            } else {
              visited.add(key)
              inStack.add(key)
            }

            item.edges.foreach(n => {
              val key = n.key
              if (!visited.contains(key)) {
                stack.push(n)
              } else if (inStack.contains(key)) {
                return true
              }
            })
          }
        }
        case _ => {}
      })
      false

    }
  }


}
