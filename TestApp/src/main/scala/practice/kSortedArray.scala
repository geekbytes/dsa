package practice

object kSortedArray extends App {

  //https://leetcode.com/problems/merge-k-sorted-lists/solution/

  import scala.collection.mutable


  object Solution3 {
    def solution(a: Array[Int]): Boolean = {
      // write your code in Scala 2.12

      //complexity = O(n) where n = length of array a

      import scala.collection.mutable

      //a set to store unmatched pairs
      val unpaired  = mutable.Set.empty[Int]


      for {
      i <- a.indices
      } {

        val value = a(i)


        if(unpaired.contains(value)) {
          //if pair found remove from unmatched set
          unpaired -= value
        } else {
          //if no pair found add to unmatched set
          unpaired += value
        }


      }

      //return true is no one is single
      unpaired.isEmpty


    }
  }


  object Solution2 {

    import scala.collection.mutable._

    type Diagram = String
    type Index = Int

    def solution(s: String): Int = {
      // write your code in Scala 2.12
      var max = -1

      if(s.length <3) return max


      val diagramFirstIndex = mutable.Map.empty[Diagram,Index]

      for {
      i <- 0 until s.length - 1
      } {
        val diagram = s"${s.charAt(i)}${s.charAt(i+1)}"

        if(diagramFirstIndex.contains(diagram)) {
          max = math.max(max,i - diagramFirstIndex(diagram))
        } else {
          diagramFirstIndex.update(diagram, i)
        }

      }

      max

    }
  }


  object Solution {
    def solution(a: Int, b: Int, c: Int): String = {
      // write your code in Scala 2.12

      case class Counter(char:Char, count:Int)


      var result  = ""


def rec(a:Int,b:Int,c:Int, counter: Option[Counter], string: String) = {



}

      result


    }
  }

  object Solution {
    def solution(a: Int, b: Int, c: Int): String = {
      // write your code in Scala 2.12

      //the idea here could be try to use teh letters with most frequency first
      //we could use a Priority Queue here where the most pending letter gets priority
      //but we nede to take care to create a diverse one

      import scala.collection.mutable._

      case class Letter(char:Char, pending:Int)

      val priorityQueue = new PriorityQueue[Letter]()(new Ordering[Letter] {
        override def compare(x: Letter, y: Letter): Int = Ordering[Int].compare(x.pending, y.pending)
      })

      priorityQueue.enqueue(Letter('a', a))
      priorityQueue.enqueue(Letter('b', b))
      priorityQueue.enqueue(Letter('c', c))


      case class Counter(char :Char, ts :Int,count :Int)

      var lastChar = Option.empty[Counter]
      val stringBuilder = new mutable.StringBuilder()
      var ts = 0

      def appendChar(char :Char): Unit = {
        stringBuilder.append(char)
        lastChar = Some(char)
      }

      val stash = new ArrayBuffer[Letter]()










      while (priorityQueue.nonEmpty) {
        var next = priorityQueue.dequeue()
        if(lastChar.fold(true)(_ == next.char)) {
          if(priorityQueue.nonEmpty) {
            stash.append(next)
            next = priorityQueue.dequeue()
          } else {
            return stringBuilder.toString()
          }
        }
        var pending = next.pending
        var i = 1

        while (1 < 3 && pending > 0) {

          appendChar(next.char)

          i += 1
          pending -= 1
        }
        if(pending > 0) {
          stash.append(next.copy(pending = pending))
        }

        if(priorityQueue.nonEmpty) {
          val interleave = priorityQueue.dequeue()
          appendChar(interleave.char)
          if(interleave.pending > 1) {
            stash.append(interleave.copy(pending = interleave.pending -1))
          }
        }

        stash.foreach(priorityQueue.enqueue(_))


      }

      stringBuilder.toString()





      while (priorityQueue.nonEmpty) {
        val next = priorityQueue.dequeue()


        var pending = next.pending
        var i = 1


        while (1 < 3 && pending > 0) {

          stringBuilder.append(next.char)

          i += 1
          pending -= 1
        }

        if(pending > 0) {
          stash.append(next.copy(pending = pending))
        }


      }

      if(stash.nonEmpty) {
        if(stash.length == 1) {
          priorityQueue.enqueue(stash.head.copy(pending = ))
        } else {
          stash.foreach(priorityQueue.enqueue(_))
        }


      } else {
        stringBuilder.toString()
      }


    }
  }

  def mergeKLists(lists: Array[ListNode]): ListNode = {

    val priorityQueue = new mutable.PriorityQueue[ListNode]()(new Ordering[ListNode] {
      override def compare(x: ListNode, y: ListNode): Int = Ordering[Int].compare(y.x, x.x)
    })

    lists.foreach(node => {
      Option(node).foreach(priorityQueue.enqueue(_))
    })

    if(priorityQueue.nonEmpty){
      val first = priorityQueue.dequeue()

      val newList = ListNode(first.x)
      var head = newList

      Option(first.next).foreach(value => priorityQueue.enqueue(value))

      while (priorityQueue.nonEmpty) {
        val next = priorityQueue.dequeue()
        Option(next.next).foreach(priorityQueue.enqueue(_))
        next.next = null
        head.next = next
        head = next
      }

      newList
    } else {
      null
    }

  }

  case class ListNode(x: Int, var next: ListNode = null)

}
