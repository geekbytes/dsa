package practice

import scala.collection.mutable

object bstIterator extends App {

  //https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/

  class BSTIterator(_root: TreeNode) {

    lazy val stack = new mutable.Stack[TreeNode]()

    def pushLeft(node: TreeNode): Unit = {

      Option(node) match {
        case Some(node) => {
          stack.push(node)
          pushLeft(node.left)
        }
        case _ => {}
      }

    }


    pushLeft(_root)


    /** @return the next smallest number */
    def next(): Int = {

      val node = stack.pop()

      pushLeft(node.right)

      node.value

    }

    /** @return whether we have a next smallest number */
    def hasNext(): Boolean = {
      stack.nonEmpty
    }

  }

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)

}
