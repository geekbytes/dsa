package practice

object TreeIsBalanced extends App {

  //https://leetcode.com/problems/balanced-binary-tree

  def isBalanced(root: TreeNode): Boolean = {

    def isBalancedRec(node: TreeNode): TreeBalance = {

      Option(node) match {
        case Some(node) => {
          (isBalancedRec(node.left), isBalancedRec(node.right)) match {
            case (left, right) if !left.balanced || !right.balanced => {
              TreeBalance(-1, false)
            }
            case (left, right) if math.abs(left.height - right.height) < 2 => {
              TreeBalance(math.max(left.height, right.height) + 1, true)
            }
            case _ => {
              TreeBalance(-1, false)
            }
          }
        }
        case _ => TreeBalance(-1, true)
      }

    }

    isBalancedRec(root).balanced

  }

  case class TreeBalance(height: Int, balanced: Boolean)

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)

}
