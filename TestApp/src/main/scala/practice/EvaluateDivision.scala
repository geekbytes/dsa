package practice

object EvaluateDivision extends App {

  import scala.collection.mutable._

  def calcEquation(equations: List[List[String]], values: Array[Double], queries: List[List[String]]): Array[Double] = {

    val map = Map.empty[String, Node]

    def getOrCreateNode(char: String): Node = {

      map.get(char) match {
        case Some(node) => node
        case _ => {
          val node = Node(char)
          map.update(char, node)
          node
        }
      }

    }

    def addEquation(i: Int): Unit = {
      val eq = equations(i)
      val dividend = getOrCreateNode(eq(0))
      val divisor = getOrCreateNode(eq(1))

      val weight = values(i)

      dividend.addEdge(weight, divisor)
      divisor.addEdge(1d / weight, dividend)

    }

    def evaluateGraph(): Unit = {

      equations.indices.foreach(addEquation)

    }

    val memoise = Map.empty[(String, String), Option[Double]]

    def calculateEquation(list: List[String]): Double = {

      val vistied = Set.empty[String]

      val a :: target :: _ = list

      def calculate(src: Node): Option[Double] = {

        if (src.value == target) return Some(1d)

        memoise.get(src.value -> target) match {
          case Some(ans) => ans
          case _ => {
            if (!vistied.contains(src.value)) {
              vistied.add(src.value)

              for {
                i <- src.edges.indices
              } {
                val (x, n) = src.edges(i)

                calculate(n) match {
                  case Some(value) => {
                    val ans = x * value
                    memoise.update(src.value -> target, Some(ans))
                    return Some(ans)
                  }
                  case None => {}
                }

              }

              memoise.update(src.value -> target, None)

            }

            None
          }
        }

      }

      if (map.contains(target)) {
        map.get(a).flatMap(calculate).getOrElse(-1.0)
      } else -1.0

    }

    evaluateGraph()

    queries.map(calculateEquation).toArray

  }

  case class Node(value: String, edges: ArrayBuffer[(Double, Node)] = ArrayBuffer.empty) {

    def addEdge(weight: Double, target: Node): Unit = {
      edges.append(weight -> target)
    }

    override def toString: String = {
      s"($value)"
    }

  }

  println(
    calcEquation(
      List(List("a", "b"), List("b", "c")),
      Array(2.0, 3.0),
      List(List("a", "c"), List("b", "a"), List("a", "e"), List("a", "a"), List("x", "x"))
    ).mkString(",")
  )

}
