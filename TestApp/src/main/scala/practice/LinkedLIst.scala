package practice

object LinkedLIst extends App {

  import scala.collection.mutable

  val linkedList = mutable.MutableList.empty[Int]

  sealed trait LinkedList {
    self =>

    def prepend(int: Int): LinkedList = {

      Cons(int: Int, self)

    }

  }

  case class Cons(head: Int, tail: LinkedList) extends LinkedList

  object LinkedList {
    def empty: LinkedList = Nil
  }

  case object Nil extends LinkedList

  linkedList += 12

  println(linkedList(0))

}
