package practice

object RemoveInvalidParanthesis extends App {

  //https://leetcode.com/problems/find-all-anagrams-in-a-string

  import scala.collection.mutable
  import scala.collection.mutable._

  def removeInvalidParentheses(s: String): List[String] = {

    val result = Set.empty[String]

    val matcher = StringMatching()

    for {
      i <- s.indices
    } {

      matcher.matchChar(s.charAt(i), i)
    }

    result.toList

  }

  case class StringMatching(stack: mutable.Stack[Int] = new mutable.Stack[Int]()) {

    val lastCompleteIndex: ArrayBuffer[Int] = ArrayBuffer.empty[Int]

    val invalidIndexes: ArrayBuffer[Int] = ArrayBuffer.empty[Int]

    def matchChar(char: Char, index: Int): Unit = {

      char match {
        case '(' => {
          stack.push(index)
        }
        case ')' => {
          if (stack.isEmpty) {
            invalidIndexes += index
          } else {
            stack.pop()
            if (stack.isEmpty) {
              lastCompleteIndex += index
            }
          }
        }

      }

    }

  }
  removeInvalidParentheses("")

}
