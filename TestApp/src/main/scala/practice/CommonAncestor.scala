package practice

object CommonAncestor extends App {

  //https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/

  def lowestCommonAncestor(root: TreeNode, p: TreeNode, q: TreeNode): TreeNode = {

    var result: TreeNode = null

    def rec(root: TreeNode): Int = {

      Option(root) match {
        case Some(root) => {

          val l = rec(root.left)
          val r = rec(root.right)

          if (l >= 2) {
            return l
          }

          if (r >= 2) {
            return r
          }

          val m = if (root.value == p.value || root.value == q.value) {
            1
          } else 0

          if (l + r + m >= 2) {
            result = root
          }

          l + r + m

        }
        case _ => 0
      }

    }

    rec(root)

    result

  }

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)

}
