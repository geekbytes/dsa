package practice.sort

object BubbleSort extends App {

  import collection.mutable._
  import scala.collection.mutable

  def sort(array: ArrayBuffer[Int]) = {

    val length = array.length

    for {
      j <- (length - 1) until 0 by -1
      i <- 0 until j
    } {

      println(j)
      val xi = array(i)
      val xiN = array(i + 1)
      if (xi > xiN) {
        swap(array, i , i+1)
      }

    }
    array

  }

  println(sort(ArrayBuffer(1, 9, 5, 3, 1, 1)))

}
