package practice

import scala.collection.mutable.ArrayBuffer

package object sort {

  def swap[A](array: ArrayBuffer[A], i: Int, j: Int): Unit = {

    val temp = array(i)
    array.update(i, array(j))
    array.update(j, temp)

  }

  def swap[A](array: Array[A], i: Int, j: Int): Unit = {

    val temp = array(i)
    array.update(i, array(j))
    array.update(j, temp)

  }

  def heapifyArray(array: ArrayBuffer[Int]): Unit = {

    val length = array.length
    for {
      i <- (length / 2 - 1) to 0 by -1
    } {
      heapify(array, length, i)
    }

  }


  @scala.annotation.tailrec
  def heapify(array: ArrayBuffer[Int], n: Int, i: Int): Unit = {

    var indexOfLargest = i
    val l = 2 * i + 1
    val r = 2 * i + 2

    if (l < n && array(l) > array(indexOfLargest)) {
      indexOfLargest = l
    }

    if (r < n && array(r) > array(indexOfLargest)) {
      indexOfLargest = r
    }

    if (indexOfLargest != i) {
      swap(array, i, indexOfLargest)
      heapify(array, n, indexOfLargest)
    }

  }

}
