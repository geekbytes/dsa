package practice.sort

import java.util.concurrent.ThreadLocalRandom

object QuickSort extends App {
  type Index = Int
  def partition[A: Ordering](array: Array[A])(l: Index, h: Index): Index = {
    val rand = ThreadLocalRandom.current().nextInt(l, h)
    val pivot = array(rand)
    var i = l
    for {
      j <- l to h
    } {
      if (Ordering[A].compare(array(j), pivot) < 0) {
        swap(array, i, j)
        i += 1
      }
    }
    swap(array, i, rand)
    i
  }

  def quickSort[A: Ordering](array: Array[A])(l: Index, h: Index): Unit = {
    if (l < h) {
      val p = partition(array)(l, h)
      quickSort(array)(l, p - 1)
      quickSort(array)(p + 1, h)
    }
  }
  def apply[A: Ordering](array: Array[A]): Unit = {
    quickSort(array)(0, array.length - 1)
  }
}
