package practice.sort

object HeapSort extends App {

  import scala.collection.mutable._

  def sort(array: ArrayBuffer[Int]): ArrayBuffer[Int] = {

    val n = array.length

    heapifyArray(array)

    for {
      i <- (n - 1) to 0 by -1
    } {
      swap(array, i, 0)
      heapify(array, i, 0)
    }

    array

  }


  def heapifyArray(array: ArrayBuffer[Int]): Unit = {

    val length = array.length
    for {
      i <- (length / 2 - 1) to 0 by -1
    } {
      heapify(array, length, i)
    }

  }


  @scala.annotation.tailrec
  def heapify(array: ArrayBuffer[Int], n: Int, i: Int): Unit = {

    var indexOfLargest = i
    val l = 2 * i + 1
    val r = 2 * i + 2

    if (l < n && array(l) > array(indexOfLargest)) {
      indexOfLargest = l
    }

    if (r < n && array(r) > array(indexOfLargest)) {
      indexOfLargest = r
    }

    if (indexOfLargest != i) {
      swap(array, i, indexOfLargest)
      heapify(array, n, indexOfLargest)
    }

  }

  println(sort(ArrayBuffer(1, 50, 6, 9, 10, 12)))

}
