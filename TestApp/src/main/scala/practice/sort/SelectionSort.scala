package practice.sort

object SelectionSort extends App {

  import collection.mutable._

  def sort(array: ArrayBuffer[Int]) = {

    val length = array.length

    var currentMin = 0
    var currentMinIndex = 0

    for {
      j <- 0 until (length)
    } {
      currentMin = array(j)
      currentMinIndex = j

      for {
        i <- (j + 1) until length
      } {
        println(i)
        val xiN = array(i)
        if (xiN < currentMin) {
          currentMin = xiN
          currentMinIndex = i
        }
      }

      array.update(currentMinIndex, array(j))
      array.update(j, currentMin)

    }
    array

  }

  println(sort(ArrayBuffer(1, 9, 5, 9, 3)))

}
