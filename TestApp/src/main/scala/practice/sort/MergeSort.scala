package practice.sort

object MergeSort extends App {

  import collection.mutable._

  def sort(array: ArrayBuffer[Int]) = {

    def mergeSort(array: ArrayBuffer[Int]): ArrayBuffer[Int] = {

      val length = array.length

      if (length > 1) {

        val mid = length / 2

        val leftArray = array.take(mid)
        val rightArray = array.drop(mid)

        println("left " + leftArray)
        println("right " + rightArray)

        mergeSort(leftArray)
        mergeSort(rightArray)

        merge(array, leftArray, rightArray)

        println("merged " + array)
        array

      } else {

        array
      }

    }

    def merge(parentArray: ArrayBuffer[Int], leftArray: ArrayBuffer[Int], rightArray: ArrayBuffer[Int]): Unit = {

      var li = 0
      var ri = 0
      var pi = 0

      while (li < leftArray.length && ri < rightArray.length) {

        if (leftArray(li) < rightArray(ri)) {
          parentArray.update(pi, leftArray(li))
          li = li + 1
        } else {
          parentArray.update(pi, rightArray(ri))
          ri = ri + 1
        }
        pi = pi + 1

      }
      while (li < leftArray.length) {
        parentArray.update(pi, leftArray(li))
        li = li + 1
        pi = pi + 1
      }
      while (ri < rightArray.length) {
        parentArray.update(pi, rightArray(ri))
        ri = ri + 1
        pi = pi + 1
      }

    }

    mergeSort(array)

  }

  println(sort(ArrayBuffer(1, 9, 5, 9, 3)))

}
