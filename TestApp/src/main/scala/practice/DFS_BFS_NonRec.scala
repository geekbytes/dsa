package practice

import practice.graph.GraphNode

import scala.collection.mutable

object DFS_BFS_NonRec extends App {

  def dfs[A](node: GraphNode[String, A])(key: A => String, traverse: A => Unit): Unit = {
    val visited = mutable.Set[String]()
    val stack = new mutable.Stack[GraphNode[String, A]]()
    stack.push(node)
    while (stack.nonEmpty) {
      val item = stack.pop()
      if (!visited.contains(key(item.value))) {
        traverse(item.value)
        visited.update(key(item.value), included = true)
        item.edges.filter(i => !visited.contains((i.key))).foreach(stack.push)
      }
    }
  }
  def bfs[A](node: GraphNode[String, A])(key: A => String, traverse: A => Unit): Unit = {
    val visited = mutable.Set[String]()
    val queue = new mutable.Queue[GraphNode[String, A]]()
    queue.enqueue(node)
    while (queue.nonEmpty) {
      val item = queue.dequeue()
      if (!visited.contains(key(item.value))) {
        traverse(item.value)
        visited.update(key(item.value), included = true)
        item.edges.filter(i => !visited.contains(key(i.value))).foreach(item => queue.enqueue(item))
      }
    }
  }
}
