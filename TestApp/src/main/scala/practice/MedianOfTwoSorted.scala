package practice

object MedianOfTwoSorted extends App {

  //https://leetcode.com/problems/product-of-array-except-self/

  def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Double = {

    val (xArray, yArray) = if (nums1.length < nums2.length) {
      nums1 -> nums2
    } else {
      nums2 -> nums1
    }

    val x = xArray.length
    val y = yArray.length

    val totalLength = nums1.length + nums2.length

    val idOdd = (totalLength & 1) == 1

    val combinedPartition = (x + y + 1) / 2

    @scala.annotation.tailrec
    def search(startIndex: Int, end: Int): Double = {

      val xPartIndex = (startIndex + end) / 2
      val yPartIndex = ((x + y + 1) / 2) - xPartIndex

      val maxLeftX = if (xPartIndex == 0) {
        Int.MinValue
      } else {
        xArray(xPartIndex - 1)
      }

      val minRightX = if (xPartIndex == x) {
        Int.MaxValue
      } else {
        xArray(xPartIndex)
      }

      val maxLeftY = if (yPartIndex == 0) {
        Int.MinValue
      } else {
        yArray(yPartIndex - 1)
      }

      val minRightY = if (yPartIndex == y) {
        Int.MaxValue
      } else {
        yArray(yPartIndex)
      }

      if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
        if (idOdd) {
          math.max(maxLeftX, maxLeftY)
        } else {
          (math.max(maxLeftX, maxLeftY) + math.min(minRightX, minRightY)) / 2.0
        }
      } else if (maxLeftX > minRightY) {
        search(startIndex, xPartIndex - 1)
      } else search(xPartIndex + 1, end)

    }

    search(0, x)

  }

}
