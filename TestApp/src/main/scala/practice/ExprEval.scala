package practice

import scala.collection.mutable.ArrayBuffer

object ExprEval extends App {

  def addOperators(num: String, target: Int): List[String] = {

    val digits: List[Int] = num.toList.map(_.asDigit)

    var result = List.empty[String]

    def rec(exp: Exp, rem: List[Int], canJoin:Boolean): Unit = {

      rem match {
        case Nil =>
          val v = exp.v
          if (v == target) {
            result = exp.s :: result
          }
        case ::(head, tl) => {

          val rhs = Val(head)
          rec(exp.add(rhs), tl, head !=0)
          rec(exp.sub(rhs), tl, head !=0)
          rec(exp.mul(rhs), tl, head !=0)
          if(canJoin) {
            rec(exp.join(head), tl, head !=0)
          }

        }
      }

    }

    digits match {
      case head :: tail => rec(Val(head), tail, head !=0)
      case Nil          => ()
    }

    result

  }

  sealed trait Exp {
    def s: String
    def mul(exp: Exp): Exp
    def add(exp: Exp): Exp
    def sub(exp: Exp): Exp
    def join(b: Int): Exp
    def v: Int
  }

  case class Val(a: Int) extends Exp {
    lazy val v: Int = a

    def s: String = a.toString

    def mul(exp: Exp): Exp = Mul(this, exp)

    def add(exp: Exp): Exp = Add(this, exp)

    def sub(exp: Exp): Exp = Sub(this, exp)

    def join(b: Int): Exp = Val(a * 10 + b)
  }

  case class Add(l: Exp, r: Exp) extends Exp {
    lazy val v: Int = l.v + r.v

    def s: String = l.s + "+" + r.s

    def mul(exp: Exp): Exp = Add(l, r.mul(exp))

    def add(exp: Exp): Exp = Add(this, exp)

    def sub(exp: Exp): Exp = Sub(this, exp)

    def join(b: Int): Exp = Add(l, r.join(b))
  }

  case class Sub(l: Exp, r: Exp) extends Exp {
    lazy val v: Int = l.v - r.v

    def s: String = l.s + "-" + r.s

    def mul(exp: Exp): Exp = Sub(l, r.mul(exp))

    def add(exp: Exp): Exp = Add(this, exp)

    def sub(exp: Exp): Exp = Sub(this, exp)

    def join(b: Int): Exp = Sub(l, r.join(b))
  }

  case class Mul(l: Exp, r: Exp) extends Exp {
    lazy val v: Int = l.v * r.v

    def s: String = l.s + "*" + r.s

    def mul(exp: Exp): Exp = Mul(l, r.mul(exp))

    def add(exp: Exp): Exp = Add(this, exp)

    def sub(exp: Exp): Exp = Sub(this, exp)

    def join(b: Int): Exp = Mul(l, r.join(b))
  }


  val r = addOperators("1000000009", 9)

  println(r.mkString("\"",",","\""))

  """[+-]?\d*(e\d*)?""".r.findFirstMatchIn("".trim)

  "."

  ArrayBuffer.empty[Int] += (4)

}
