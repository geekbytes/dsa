package practice

object NoOfIslands extends App {

  def numIslands(grid: Array[Array[Char]]): Int = {

    def visit(x: Int, y: Int): Unit = {
      grid(x).update(y, '0')
      if (y + 1 < grid(x).length && grid(x)(y + 1) != '0') {
        visit(x, y + 1)
      }
      if (x + 1 < grid.length && grid(x + 1)(y) != '0') {
        visit(x + 1, y)
      }
      if (y - 1 >= 0 && grid(x)(y - 1) != '0') {
        visit(x, y - 1)
      }
      if (x - 1 >= 0 && grid(x - 1)(y) != '0') {
        visit(x - 1, y)
      }
    }

    var counter = 0

    for {
      i <- grid.indices
      j <- grid(i).indices
    } {

      if (grid(i)(j) != '0') {

        counter += 1

        visit(i, j)

      }

    }

    counter

  }

}
