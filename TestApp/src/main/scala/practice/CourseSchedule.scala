package practice

import practice.graph.{Graph, GraphNode}

object CourseSchedule extends App {

  import scala.collection.mutable._

  def findOrder(numCourses: Int, prerequisites: Array[Array[Int]]): Array[Int] = {

    val graph = Graph.empty[Int, Int]

    for {
      i <- 0 until numCourses
    } {
      val node = GraphNode.int(i)
      graph.addNode(node)
    }

    prerequisites.foreach(link => {
      graph.link(link(0), link(1))
    })

    val result = new ArrayBuffer[Int]()

    graph.dfsAllNodesWithCycle(true)(node => result += node.value)
    println(result)

    result.toArray

  }

  findOrder(3, Array(Array(0, 1), Array(0, 2), Array(1, 2)))

}
