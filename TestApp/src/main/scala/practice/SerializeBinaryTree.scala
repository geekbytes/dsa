package practice

object SerializeBinaryTree extends App {

  import scala.collection.mutable

  //https://leetcode.com/problems/serialize-and-deserialize-binary-tree/

  class Codec {

    // Encodes a list of strings to a single string.
    def serialize(root: TreeNode): String = {

      def doBfs(root: TreeNode, s: mutable.StringBuilder): mutable.StringBuilder = {

        Option(root) match {
          case Some(root) => {
            s.append(root.value + ",")
            doBfs(root.left, s)
            doBfs(root.right, s)

          }
          case None => {
            s.append("null,")
          }
        }

      }

      doBfs(root,new mutable.StringBuilder()).toString()

    }

    // Decodes a single string to a list of strings.
    def deserialize(s: String): TreeNode = {


      val list = s.split(",").toList


      def doDeserialize(s:List[String]):(List[String],TreeNode) = {

        if(s.head == "null") {

          s.tail -> null


        } else {
          val (ls,left) = doDeserialize(s.tail)
          val (rem,right) = doDeserialize(ls)
          rem -> TreeNode(s.head.toInt,left,right)
        }





      }

      doDeserialize(list)._2





    }
  }

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)

}
