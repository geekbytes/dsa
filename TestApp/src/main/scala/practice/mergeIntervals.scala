package practice

import scala.collection.mutable.ArrayBuffer

object mergeIntervals extends App {

  //https://leetcode.com/problems/merge-intervals/

  import scala.util.Sorting

  def merge(intervals: Array[Array[Int]]): Array[Array[Int]] = {

    if (intervals.nonEmpty) {
      Sorting.quickSort(intervals)(new Ordering[Array[Int]] {
        override def compare(x: Array[Int], y: Array[Int]): Int = Ordering[Int].compare(x(0), y(0))
      })

      val list = ArrayBuffer(intervals(0))

      for {
        i <- 1 until intervals.length
      } {

        val Array(start, end) = intervals(i)

        val last = list.last

        if (last(1) >= start) {
          if (last(1) < end) {
            last.update(1, end)
          }
        } else {
          list += intervals(i)
        }
      }

      list.toArray
    } else {
      intervals
    }

  }

}
