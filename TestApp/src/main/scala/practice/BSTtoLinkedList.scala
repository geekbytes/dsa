package practice

object BSTtoLinkedList extends App {

  //https://leetcode.com/problems/convert-binary-search-tree-to-sorted-doubly-linked-list/

  def treeToDoublyList(root: Node): Node = {

    def rec(node: Node): (Node, Node) = {

      val (first, left) = Option(node.left) match {
        case Some(left) => rec(left)
        case None       => (node , null)
      }

      val (right, last) = Option(node.right) match {
        case Some(right) => rec(right)
        case None        => (null , node)
      }

      Option(left).foreach(_.right = node)
      node.left = left
      node.right = right
      Option(right).foreach(_.left = node)

      first -> last

    }

    Option(root) match {
      case Some(root) => {
        val (first, last) = rec(root)

        first.left = last
        last.right = first

        first
      }
      case None => null
    }

  }

  case class
  Node(value: Int, var left: Node, var right: Node)

}
