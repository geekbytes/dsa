package practice


object GenerateParanthesis extends App {

  //https://leetcode.com/problems/generate-parentheses/

  import scala.collection.mutable._

  def generateParenthesis(n: Int): List[String] = {

    val result = Set.empty[String]


    def rec(s:String, opened: Int, closed: Int): Unit = {

      if (opened == 0 && closed == 0) {
        result.add(s)
        return
      }

      if (opened > 0) {
        rec(s + "(", opened - 1, closed)
      }
      if (closed > 0 && closed > opened) {
        rec(s + ")", opened, closed - 1)
      }

    }

    rec("", n, n)

    result.toList

  }

  generateParenthesis(3)

}
