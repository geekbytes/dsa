package practice

object Arrays extends App {

  import collection.mutable._
  import scala.collection.mutable
  type Dependecies = Map[Int, Array[Int]]
  val testCase: mutable.Map[Int, Array[Int]] = Map(
    0 -> Array(),
    1 -> Array(0),
    2 -> Array(0),
    3 -> Array(1, 2),
    4 -> Array(3)
  )

  def packageDependencies(projectDependencies: Dependecies): Seq[Int] = {

    val result = ArrayBuffer.empty[Int]

    val visited: Set[Int] = Set.empty
    val visiting: Set[Int] = Set.empty

    def dfs(node: Int): Unit = {
      if (visiting.contains(node)) {
        throw new Exception("cyclic dependency")
      }
      visiting.add(node)
      if (!visited.contains(node)) {
        val dependencies = projectDependencies.getOrElse(node, Array.emptyIntArray)
        dependencies.foreach(dfs)
        result.append(node)

        visited.add(node)
      }
      visiting.remove(node)
    }

    def bfs(node: Int) = {
      if (visiting.contains(node)) {
        throw new Exception("cyclic dependency")
      }
      visiting.add(node)
      if (!visited.contains(node)) {
        result.append(node)
        val dependencies = projectDependencies.getOrElse(node, Array.emptyIntArray)
        dependencies.foreach(dfs)
        visited.add(node)
      }
      visiting.remove(node)
    }

    projectDependencies.keysIterator.foreach(bfs)

    result

  }

  println(packageDependencies(testCase).mkString("[", ",", "]"))

}
