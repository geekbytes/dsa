package practice

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

// you can write to stdout for debugging purposes, e.g.
// println("this is a debug message")

//https://app.codility.com/c/run/7YHKQK-Z9K/

object Solution {

  def solution(s: String, c: String): Int = {

    val header +: data = s.split("\n").toSeq
    val columnIndex = header.split(",").indexOf(c)

    data.foldLeft(Int.MinValue)({
      case (int, row) => {
        val cur = row.split(",")(columnIndex).toInt
        math.max(cur, int)
      }
    })

  }
}

object CleaningRobot {

  def solution(r: Array[String]): Int = {
    // write your code in Scala 2.12

    val maxX = r.length - 1
    val maxY = r(0).length - 1

    def getNext(position: (Int, Int),
                direction: Char,
                initialDir: Char,
                journey: Map[(Int, Int), Set[Char]],
                distance: Int): Int = {
      val (x, y) = position
      val (nextDir, (xx, yy)) = direction match {
        case 'R' =>
          ('D', {
            x -> (y + 1)
          })
        case 'D' =>
          ('L', {
            (x + 1) -> (y)
          })
        case 'L' =>
          ('U', {
            (x) -> (y - 1)
          })
        case 'U' =>
          ('R', {
            (x - 1) -> (y)
          })
      }

      if (xx > maxX || xx < 0 || yy > maxY || yy < 0 || r(xx)(yy) == 'X' || journey
            .get(xx -> yy)
            .fold(false)(_.contains(direction))) {
        if (initialDir == nextDir) distance
        else {
          getNext(x -> y, nextDir, initialDir, journey.updated(x -> y, journey(x -> y) + nextDir), distance)
        }
      } else {
        getNext(
          xx -> yy,
          direction,
          direction,
          journey.updated(xx -> yy, journey.getOrElse(xx -> yy, Set.empty[Char]) + direction), {
            if (journey.contains(xx -> yy)) distance else distance + 1
          }
        )
      }

    }

    getNext(0 -> 0, 'R', 'R', Map((0 -> 0, Set('R'))), 1)

  }

}

object ExpenditureSolution {
  def solution(a: Array[Int]): Int = {
    // write your code in Scala 2.12


    val pq = new mutable.PriorityQueue[Int]()

    a.foldLeft(0 -> 0)({
      case ((rel,total), cost) => {
        val newTotal = total + cost
        val exp = cost * -1
        if(newTotal < 0 ) {
          if(pq.nonEmpty) {

            val maxCost = pq.head

            if(maxCost > exp) {
              pq.dequeue()
              (rel + 1 , newTotal + maxCost)
            } else {
              (rel + 1 , total)
            }

          } else {
            pq.enqueue(exp)
            rel -> newTotal
          }
        } else {
          if(cost < 0) {
            pq.enqueue(exp)
          }
          rel -> newTotal
        }
      }
    })._1





  }
}
