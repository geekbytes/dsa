package practice

object AddTwoNumbers extends App {

  def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {

    var result: ListNode = null

    @scala.annotation.tailrec
    def rec(carry: Int, l1: ListNode, l2: ListNode, lastNode: ListNode): Unit = {

      if (carry != 0 || Option(l1).isDefined || Option(l2).isDefined) {
        val (o1, o2) = (Option(l1).fold(0)(_.x), Option(l2).fold(0)(_.x))
        val sum = carry + o1 + o2

        println(sum)
        val node = ListNode(sum % 10, null)
        Option(lastNode) match {
          case Some(_) => lastNode.next = node
          case None => {
            result = node
          }
        }

        rec(sum / 10, Option(l1).map(_.next).orNull, Option(l2).map(_.next).orNull, node)
      } else {}

    }

    rec(0, l1, l2, result)

    Option(result).getOrElse(ListNode(0, null))

  }

  case class ListNode(x: Int, var next: ListNode)

}
