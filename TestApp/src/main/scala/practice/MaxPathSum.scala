package practice

object MaxPathSum extends App {

  //https://leetcode.com/problems/binary-tree-maximum-path-sum

  def maxPathSum(root: TreeNode): Int = {

    var maxPath: Option[Int] = None
    var maxPathNode: TreeNode = null

    def maxPathSumRec(root: TreeNode): Option[Int] = {

      Option(root) match {
        case Some(root) => {

          val left = maxPathSumRec(root.left)
          val righ = maxPathSumRec(root.right)

          val sum = ((left),(righ)) match {
            case (Some(l),Some(r)) =>  math.max(l , r) + root.value
            case (Some(l),_) =>  l + root.value
            case (_,Some(r)) =>   r + root.value
            case (_,_) =>   root.value
          }

         if (maxPath.fold(true)(_ < sum) ){
            maxPath = Some(sum)
            maxPathNode = root
          }
          Some(sum)
        }
        case _ => None
      }

    }

    maxPathSumRec(root)

    maxPath.getOrElse(0)

  }

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)

}
