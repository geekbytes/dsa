package practice

import java.util.Comparator

import practice.UnionFind.Node

import scala.annotation.tailrec
import scala.collection.mutable

case class UnionFind[K, V](nodes: mutable.Map[K, Node[K, V]])(implicit comparor: Comparator[K]) {

  final def findRoot(key: K): Node[K, V] = {
    nodes(key) match {
      case node if comparor.compare(key, node.parent) == 0 => node
      case node => {
        val root = findRoot(node.parent)
        nodes.update(key, node.copy(parent = root.parent))
        root
      }
    }
  }

  private def merge(xRoot: Node[K, V], yRoot: Node[K, V]): Node[K, V] = {
    nodes.update(yRoot.parent, yRoot.copy(parent = xRoot.parent))
    val newRoot = xRoot.copy(rank = xRoot.rank + yRoot.rank)
    nodes.update(xRoot.parent, newRoot)
    newRoot
  }

  def union(x: K, y: K): Node[K, V] = {
    val xRoot: Node[K, V] = findRoot(x)
    val yRoot: Node[K, V] = findRoot(y)

    if (comparor.compare(xRoot.parent, yRoot.parent) != 0) {
      if (xRoot.rank > yRoot.rank) {
        merge(xRoot, yRoot)
      } else {
        merge(yRoot, xRoot)
      }
    } else {
      xRoot
    }

  }

}

object UnionFind {

  case class Node[K, V](parent: K, value: V, rank: Int = 0)

  def apply[K, V](items: mutable.Seq[(K, V)])(implicit compare: Comparator[K]): UnionFind[K, V] =
    new UnionFind(mutable.Map(items.map(item => item.copy(_2 = Node(item._1, item._2))): _*))

}

case class UnionFindIterative(n: UnionFindIterative.Size) {
  import scala.collection.mutable._
  import UnionFindIterative._

  val parent: Array[Parent] = Array.tabulate(n)(identity)
  val size: Array[Size] = Array.fill(n)(1)

  def findRoot(a: Int):Parent = {

    var i = a

    while (parent(i) != i) {
      i = parent(i)
    }

    val root = i

    i = a

    while (i != root) {
      val p = parent(i)
      parent.update(i, root)
      i = p
    }
    root

  }


  def union(a:Int, b:Int):Boolean = {

    val aR = findRoot(a)
    val bR = findRoot(b)

    if(aR == bR) {
      return false
    } else {

      if(size(aR) < size(bR)) {
        parent.update(aR, bR)
        size.update(bR, size(bR) + size(aR))
      } else {
        parent.update(bR, aR)
        size.update(aR, size(bR) + size(aR))
      }

    }

    true

  }


}

object UnionFindIterative {
  type Size = Int
  type Parent = Int
}
