package practice.bits

object BitInsert extends App {

  val j = 6
  val i = 2

  val n = Integer.parseInt("10010001010", 2)
  val m = Integer.parseInt("10101", 2)

  val max = ~0L /* All 1’s */
  val left = max << j; //32 bit with all 1 except right j digits 0
  val oneAndiZeros = 1 << i // regex = 10{i}
  val right = (oneAndiZeros - 1);
  val mask = left | right

  val maskedN = n & mask
  val res = maskedN | (m << i)

  left.asBinaryStr
  right.asBinaryStr
  mask.asBinaryStr
  maskedN.asBinaryStr

  res.asBinaryStr


  val regex = """(\d)+(?:\.(\d+))?""".r

  "2.67" match {
    case regex(l,r) => println(r)
  }

}

