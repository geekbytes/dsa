package practice

package object bits {

  implicit class BitOps(private val int:Int) {
    def asBinaryStr = println(int.toBinaryString)
  }

  implicit class BitOpsLong(private val lon:Long) {
    def asBinaryStr = println(lon.toBinaryString)
  }

}
