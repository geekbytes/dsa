package practice

object ArraySum extends App {

  import scala.collection.mutable._

  def twoSum(nums: Array[Int], target: Int): Array[Int] = {

    val map = Map.empty[Int,Int]

    for {
      i <- nums.indices
    } {
      map.get(nums(i)) match {
        case Some(pair) => {
          return Array(pair,i)
        }
        case _ => map.update(target - nums(i), i)
      }
    }

    Array.empty[Int]
  }

  twoSum(Array(2, 7, 11, 15),9)

}
