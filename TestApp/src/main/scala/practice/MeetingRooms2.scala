package practice

object MeetingRooms2 extends App {

  //https://leetcode.com/problems/meeting-rooms-ii/

  import scala.collection.mutable
  import scala.util.Sorting

  def minMeetingRooms(intervals: Array[Array[Int]]): Int = {

    if (intervals.length > 0) {

      val queue = new mutable.PriorityQueue[Int].reverse

      Sorting.quickSort(intervals)(new Ordering[Array[Int]] {
        override def compare(x: Array[Int], y: Array[Int]): Int = Ordering[Int].compare(x(0), y(0))
      })

      queue.enqueue(intervals(0)(1))

      for {

        i <- 1 until intervals.length

      } {

        if (intervals(i)(0) >= queue.head) {

          queue.dequeue()

        }

        queue.enqueue(intervals(i)(1))

      }

      queue.length
    } else 0

  }

  minMeetingRooms(Array(Array(0, 30), Array(5, 10), Array(15, 20)))

}
