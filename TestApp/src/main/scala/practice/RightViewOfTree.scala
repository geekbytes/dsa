package practice

import scala.collection.mutable._

object RightViewOfTree extends App {

  //https://leetcode.com/problems/binary-tree-right-side-view/

  def rightSideView(root: TreeNode): List[Int] = {

    val array = Map.empty[Int, Int]

    var max_depth = -1

    def update(level: Int, node: TreeNode): Unit = {

      Option(node) match {

        case Some(node) => {

          array.update(level, node.value)

          max_depth = math.max(level, max_depth)

          update(level + 1, node.left)
          update(level + 1, node.right)

        }
        case _ => ()

      }

    }

    update(0, root)

    var i = 0

    var seq = new Array[Int](max_depth + 1)
    while (i <= max_depth) {

      array.get(i) match {
        case Some(value) => {
          seq.update(i, value)
          i = i + 1
        }
        case None => {
          i = i + 1
        }
      }

    }

    seq.toList

  }

  case class TreeNode(value: Int, left: TreeNode, right: TreeNode)
}
