package practice

//https://leetcode.com/problems/exclusive-time-of-functions/submissions/

object ExclusiveTimeOfFunctions extends App {
  //https://leetcode.com/problems/exclusive-time-of-functions/submissions/

  def exclusiveTime(n: Int, logs: List[String]): Array[Int] = {

    import scala.collection.mutable._

    val res = Array.fill(n)(0)

    val stack = new Stack[(Int, Option[Int])]()

    def execLog(logs: List[String]): Unit = {

      logs match {

        case head :: rest => {

          val logItems = head.split(":")

          val (t, state, ts) = (logItems(0).toInt, logItems(1), logItems(2).toInt)

          state match {
            case "start" => {

              if (stack.nonEmpty) {
                stack.pop() match {
                  case (elapsed, Some(startTs)) => {
                    stack.push((elapsed + (ts - startTs)) -> None)
                  }
                  case _ => {}
                }

              }

              stack.push(0 -> Some(ts))

            }

            case "end" => {

              stack.pop() match {
                case (elapsed, Some(startTs)) => {
                  res.update(t.toInt, res(t.toInt) + elapsed + (ts - startTs) + 1)
                }
                case _ => {}
              }

              if (stack.nonEmpty) {
                stack.pop() match {
                  case (elapsed, None) => {
                    stack.push(elapsed -> Some(ts + 1))
                  }
                  case _ => {}
                }
              }
            }
          }

          execLog(rest)

        }
        case _ => {}

      }

    }

    execLog(logs)

    res

  }

  exclusiveTime(2, List("0:start:0", "1:start:2", "1:end:5", "0:end:6"))

}
