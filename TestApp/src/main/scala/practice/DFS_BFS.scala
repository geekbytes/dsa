package practice

object DFS_BFS extends App {

  import collection.mutable._
  import scala.collection.mutable
  type Dependecies = Map[Int, Array[Int]]

  def packageDependencies(projectDependencies: Dependecies): Seq[Int] = {



    projectDependencies.keysIterator.foreach(bfs)

    result

  }

  val testCase: mutable.Map[Int, Array[Int]] = Map(
    0 -> Array(),
    1 -> Array(0),
    2 -> Array(0),
    3 -> Array(1, 2),
    4 -> Array(3)
  )

  println(packageDependencies(testCase).mkString("[", ",", "]"))

}
