package practice

import scala.collection.mutable
import scala.collection.mutable._
import scala.util.control.Breaks._

object WorkBreak extends App {

  //https://leetcode.com/problems/word-break

  def wordBreak(s: String, wordDict: List[String]): Boolean = {

    val dic = wordDict.toSet
    val visited = Set.empty[Int]

    val start = new mutable.Queue[Int]()
    start.enqueue(0)

    while (start.nonEmpty) {
      val i = start.dequeue()

      if(!visited.contains(i)) {
        for {
          j <- i to s.length
        } {
          if (dic.contains(s.substring(i, j))) {
            start.enqueue(j)
            if (j == s.length) return true
          }
        }
        visited.add(i)
      }


    }
    false

  }


  def wordBreakDP(s: String, wordDict: List[String]): Boolean = {

    val dic = wordDict.toSet

    val array = Array.fill(s.length + 1)(false)

    array.update(0, true)

    for {
      i <- 1 until s.length
    } {

      breakable {
        for {
          j <- 0 until i
        } {

          if (array(j) && dic.contains(s.substring(j, i))) {
            array.update(i, true)
            break
          }


        }
      }


    }

    array(s.length)


  }


}
