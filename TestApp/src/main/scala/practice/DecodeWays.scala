package practice

object DecodeWays extends App {

  //https://leetcode.com/problems/task-scheduler/

  //

  def numDecodings(s: String): Int = {

    val ns = "x" + s

    val array = new Array[Int](ns.length)
    array.update(0, 1)
    if (array(1) != '0') {
      array.update(1, 1)
    } else {
      array.update(1, 0)
    }

    for {
      i <- 2 until ns.length
    } {

      val singleOption = {
        if (array(i) != '0') {
          array(i - 1)
        } else 0
      }

      val doubleCount = {

        val ds: String = s"${ns.charAt(i - 1)}${ns.charAt(i)}"

        val int = ds.toInt

        if (int > 9 && int < 27) {
          array(i - 2)
        } else 0

      }

      array.update(i, singleOption + doubleCount)

    }

    array(ns.length - 1)

  }

}
