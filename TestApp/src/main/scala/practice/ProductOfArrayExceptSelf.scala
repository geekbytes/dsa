package practice

object ProductOfArrayExceptSelf extends App {

  //https://leetcode.com/problems/product-of-array-except-self/

  def productExceptSelf(nums: Array[Int]): Array[Int] = {

    if (nums.nonEmpty) {
      val result = new Array[Int](nums.length)

      result.update(0, 1)

      for {
        i <- 1 until result.length
      } {
        result.update(i, result(i - 1) * nums(i - 1))
      }

      var r = 1

      for {
        j <- (result.length - 1) to 0 by -1
      } {

        val newVal = result(j) * r
        r = r * nums(j)
        result.update(j, newVal)

      }

      result
    } else nums

  }

}
