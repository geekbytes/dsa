import scala.collection.mutable
import scala.collection.mutable._
import scala.util.control.Breaks._

package object practice {

  mutable.Map.empty.updated(2, 8)

  new mutable.StringBuilder()

  (new ArrayBuffer[Int]()).append()

  Array.fill(3)(-1).length

  new Stack[Int]()

  Set.empty[Int].update(1, true)

  Some()

  breakable {}

  's'.isLetterOrDigit

 val s: String = ""

  s.drop(1)

  s.split("/").dropWhile(_.isEmpty)

  s.span(_ == '/')

  "".toList match {
    case 'a' +: _ =>
  }


'f'.asDigit


  "".toList match {
    case 'f' +: _ =>
  }

  ""

  /*Array.empty[Int] match {
    case 1 +: d => {

    }
  }*/


  ArrayBuffer.empty[Int]


  new StringBuilder()




  case class Letter(value: Char, var left: Letter = null, var right: Letter = null)

  Array.empty[String].apply(0).apply(0)
  new Array(1)


  MutableList.empty[Int] += 3


  LinkedList.empty[String]

  "".split("")

  object Solution {

    def mult(num: Int, x:Int):Int = {

      val s = num * x

      s


    }


    case class DigitAccess(str:String) {

      lazy val l = str.length

      def get(index:Int):Int = {

        if(l-1 - index >= 0) {
          str.charAt(l-1 - index)
        } else 0


      }

    }


    def multiply(num1: String, num2: String): String = {

      val a = DigitAccess(num1)
      val b = DigitAccess(num2)

      val (m,x) = if(a.l > b.l ) {
        a -> b
      } else b -> a

      var sum = 0D

      for {
        i <- 0 until x.l
      } {
        for {
          j <- 0 until m.l
        } {
          sum = sum + (mult(m.get(j),x.get(i)) * math.pow(10 ,i))
        }
      }

      sum.toString

    }
  }


}
