package practice

object PallindromePair extends App {

  import scala.collection.mutable._

  def palindromePairs(words: Array[String]): List[List[Int]] = {

    def isPalindrome(s: String, ii: Int, jj: Int): Boolean = {

      println("--")
      println(s)
      println(ii)
      println(jj)

      var i = ii
      var j = jj
      while (i < j) {
        if (s.charAt(i) == s.charAt(j)) {
          i += 1
          j -= 1
        } else return false
      }
      println("true")
      println("--")
      return true

    }

    var result = List.empty[List[Int]]

    case class Node(value: Char,
                    root: Boolean = false,
                    map: Map[Char, Node] = Map.empty,
                    var index: Option[Int] = None,
                    var other: Seq[(Int, Int)] = Seq.empty) {

      def add(s: String, i: Int, si: Int): Unit = {
        if (i >= 0) {
          val c = s.charAt(i)
          val node = if (map.contains(c)) {
            map(c)
          } else {
            val node = Node(c)
            map.update(c, node)
            node
          }
          other = other :+ (si, i)
          node.add(s, i - 1, si)
        } else {
          index = Some(si)
        }
      }

      def find(s: String, i: Int, si: Int, sl: Int): Unit = {
        if (i == sl) {
          if (index.isDefined) {
            if (index.get != si) {
              result = result :+ List(si, index.get)
            }
          }
          other.foreach({
            case (sii, j) =>
              if (isPalindrome(words(sii), 0, j)) {
                result = result :+ List(si, sii)
              }
          })

        } else {
          val c = s.charAt(i)
          if (map.contains(c)) {
            map(c).find(s, i + 1, si, sl)
          }
        }
      }
    }

    val root = Node('_', true)

    for {
      i <- words.indices
    } {
      root.add(words(i), words(i).length - 1, i)
    }

    for {
      i <- words.indices
    } {
      println(">>>" + words(i))
      root.find(words(i), 0, i, words(i).length)
    }

    result

  }

  palindromePairs(Array("abcd", "dcba", "lls", "s", "sssll"))

}
