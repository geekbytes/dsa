package practice

object LCS extends App {

  import collection.mutable._
  import scala.collection.mutable

  trait LCS {

    def apply(a: String, b: String): Int

  }

  object recursion extends LCS {

    def apply(a: String, b: String): Int = {

      val an: Int = a.length
      val bn: Int = b.length

      def lcs(i: Int, j: Int): Int = {
        if (i < an && j < bn) {
          if (a(i) == b(j)) {
            1 + lcs(i + 1, j + 1)
          } else {
            math.max(lcs(i, j + 1), lcs(i + 1, j))
          }
        } else 0
      }

      lcs(0, 0)

    }
  }

  object memoization extends LCS {

    def apply(a: String, b: String): Int = {

      val map = mutable.Map.empty[(Int, Int), Int]

      val an: Int = a.length
      val bn: Int = b.length

      def lcs(i: Int, j: Int): Int = {
        if (i < an && j < bn) {
          map.get(i -> j) match {
            case Some(mem) => mem
            case None => {
              val result = {
                if (a(i) == b(j)) {
                  1 + lcs(i + 1, j + 1)
                } else {
                  math.max(lcs(i, j + 1), lcs(i + 1, j))
                }
              }
              map += ((i -> j, result))
              result
            }
          }

        } else 0
      }

      lcs(0, 0)

    }
  }

  object dynamic extends LCS {

    def apply(a: String, b: String): Int = {

      val map = mutable.Map.empty[(Int, Int), Int]

      val an: Int = a.length
      val bn: Int = b.length

      val string = new mutable.StringBuilder()

      for {
        i <- 0 until an
        j <- 0 until bn
      } {
        if (i == 0 || j == 0) {
          map += ((i -> j, 0))
        } else {
          if (a(i) == b(j)) {
            1 + map(i -> j)
            string.append(a(i))
          } else {
            val up = map(i -> (j - 1))
            val down = map((i - 1) -> j)
            math.max(up, down)
          }
        }

      }

      println(string)

      map((an - 1) -> (bn - 1))

    }
  }

  println()

}
