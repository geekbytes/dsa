package practice

object IntToWords extends App {

  //https://leetcode.com/problems/product-of-array-except-self/

  def numberToWords(num: Int): String = {

    val ones = Map(
      1 -> "One",
      2 -> "Two",
      3 -> "Three",
      4 -> "Four",
      5 -> "Five",
      6 -> "Six",
      7 -> "Seven",
      8 -> "Eight",
      9 -> "Nine"
    )

    val teens = Map(
      11 -> "Eleven",
      12 -> "Twelve",
      13 -> "Thirteen",
      14 -> "Fourteen",
      15 -> "Fifteen",
      16 -> "Sixteen",
      17 -> "Seventeen",
      18 -> "Eighteen",
      19 -> "Nineteen"
    )

    val tens = Map(
      10 -> "Ten",
      20 -> "Twenty",
      30 -> "Thirty",
      40 -> "Fourty",
      50 -> "Fifty",
      60 -> "Sixty",
      70 -> "Seventy",
      80 -> "Eighty",
      90 -> "Ninety"
    )

    def onePart(a: Int) = {

      val nums = Seq.empty[String]




      if (a != 0) {
        val (thirdDigit, twoDigits) = (a / 100) -> a % 100

        s"${if (thirdDigit == 0) ""
        else s"${ones(thirdDigit)} Hundred"}${if (twoDigits == 0) ""
        else {
          " " + teens.getOrElse(twoDigits, tens.getOrElse(twoDigits, {
            val (secondDigit, firstDigit) = ((twoDigits / 10) * 10) -> (twoDigits % 10)
            s"${tens(secondDigit)} ${ones(firstDigit)}"

          }))
        }}"
      }

    }

    val BILLION = 1000000000
    val MILLION = 1000000
    val THOUSANDS = 1000
    val (billions, millions, thousans, end) =
      ((num / BILLION), (num % BILLION) / MILLION, (num % MILLION) / THOUSANDS, num % THOUSANDS)

    s"${if (billions > 0) {
      onePart(billions) + " Billion"
    } else ""}${if (millions > 0) {
      " " + onePart(millions) + " Million"
    } else ""}${if (thousans > 0) {
      " " + onePart(thousans) + " Thousand"
    } else ""}${if (end > 0) {
      " " + onePart(end)
    } else ""}"

  }

}
