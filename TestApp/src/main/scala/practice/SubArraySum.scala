package practice

object SubArraySum extends App {

  //https://leetcode.com/problems/balanced-binary-tree

  import scala.collection.mutable._

  def subarraySum(nums: Array[Int], k: Int): Int = {

    var count = 0
    var sum = 0

    val map = Map.empty[Int, Int]
    map.update(0, 1)
    for {
      i <- nums.indices
    } {
      sum = sum + nums(i)

      if (map.contains(sum - k)) {
        count += map(sum - k)
      }

      map.update(sum, map.getOrElse(sum, 0) + 1)

    }

    count

  }

}
