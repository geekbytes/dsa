package practice

import scala.collection.mutable.ArrayBuffer

object BinaryTree extends App {

  val test = List(4, 2, 5, 6, 1, 3)
  val tree = test.foldLeft(BinaryTree.empty[Int]) {
    case (tree, item) => tree.insert(item)
  }

  abstract class BinaryTree[+A] {

    def insert[AA >: A](item: AA)(implicit ordering: Ordering[AA]): BinaryTree[AA]

  }

  case class NonEmptyTree[A](var left: BinaryTree[A], var value: A, var right: BinaryTree[A])(
      implicit ordering: Ordering[A]
  ) extends BinaryTree[A] {
    override def insert[AA >: A](item: AA)(implicit ordering: Ordering[AA]): BinaryTree[AA] = {
      val comparision = ordering.compare(item, value)
      if (comparision < 0) {
        NonEmptyTree(left.insert(item), value, right)
      } else if (comparision > 0) {
        NonEmptyTree(left, value, right.insert(item))
      } else this
    }
  }

  object BinaryTree {
    def empty[A]: BinaryTree[A] = EmptyTree

    def singleton[A](item: A)(implicit ordering: Ordering[A]): BinaryTree[A] = EmptyTree.insert(item)
  }

  case object EmptyTree extends BinaryTree[Nothing] {
    override def insert[AA >: Nothing](item: AA)(implicit ordering: Ordering[AA]): BinaryTree[AA] =
      NonEmptyTree[AA](EmptyTree, item, EmptyTree)
  }

  object Traverse {

    def showInOrder[A](tree: BinaryTree[A]): ArrayBuffer[A] = {

      val array = ArrayBuffer.empty[A]

      def inOrder(tree: BinaryTree[A]): Unit = {
        tree match {
          case NonEmptyTree(left, value, right) => {
            inOrder(left)
            array += value
            inOrder(right)
          }
          case _ => ()
        }
      }
      inOrder(tree)

      array

    }

    def showPreOrder[A](tree: BinaryTree[A]): ArrayBuffer[A] = {

      val array = ArrayBuffer.empty[A]

      def preOrder(tree: BinaryTree[A]): Unit = {
        tree match {
          case NonEmptyTree(left, value, right) => {
            array += value
            preOrder(left)
            preOrder(right)
          }
          case _ => ()
        }
      }

      preOrder(tree)

      array

    }

    def showPostOrder[A](tree: BinaryTree[A]): ArrayBuffer[A] = {

      val array = ArrayBuffer.empty[A]

      def postOrder(tree: BinaryTree[A]): Unit = {
        tree match {
          case NonEmptyTree(left, value, right) => {
            postOrder(left)
            postOrder(right)
            array += value
          }
          case _ => ()
        }
      }

      postOrder(tree)

      array

    }

  }

  println(tree)

  println(Traverse.showInOrder(tree))

  println(Traverse.showPreOrder(tree))

  println(Traverse.showPostOrder(tree))

}
