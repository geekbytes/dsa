package practice

object IsPallindrome extends App {

  //https://leetcode.com/problems/merge-k-sorted-lists/solution/

  def isPalindrome(s: String) = {

    def isPalindomeRange(l: Int, h: Int): Option[(Int, Int)] = {

      for {
        k <- 0 until ((h - l) / 2)
      } {

        val i = l + k
        val j = h - k

        if (s.charAt(i) != s.charAt(j)) {
          println((i, j, false))
          return Some(i, j)
        }

        println((i, j, true))

      }

      None

    }

    isPalindomeRange(0, s.length - 1) match {
      case Some((i, j)) => {
        isPalindomeRange(i + 1, j).isEmpty || isPalindomeRange(i, j - 1).isEmpty
      }
      case _ => true
    }

  }

  println(isPalindrome("abbac"))

}
