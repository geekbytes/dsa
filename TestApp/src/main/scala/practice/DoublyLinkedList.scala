package practice

import DoublyLinkedList._

case class DoublyLinkedList[A](var head: Option[DoublyLinkedListNode[A]], var tail: Option[DoublyLinkedListNode[A]])(var size: Int = 0) {

  def addNode(node: DoublyLinkedListNode[A]) = {

    head match {
      case Some(head) => {
        head.prev = Some(node)
      }
      case _ => {
        tail = Some(node)
      }
    }
    node.next = head
    node.prev = None

    head = Some(node)

    size += 1

  }

  def addTail(node: DoublyLinkedListNode[A]) = {
    tail match {
      case Some(tail) => {
        tail.next = Some(node)
      }
      case _ => {
        head = Some(node)
      }
    }

    node.prev = tail
    node.next = None

    tail = Some(node)

    size += 1
  }

  def removeTail(): Option[DoublyLinkedListNode[A]] = {

    tail.map(tail => {

      removeNode(tail)

      tail
    })

  }

  def removeNode(node: DoublyLinkedListNode[A]) = {

    node.prev match {
      case Some(prev) => prev.next = node.next
      case _ => {
        head = node.next
      }
    }

    node.next match {
      case Some(next) => next.prev = node.prev
      case _ => {
        tail = node.prev
      }
    }

    size -= 1

  }

}

object DoublyLinkedList {
  def empty[A]: DoublyLinkedList[A] = DoublyLinkedList[A](head = None, tail = None)(0)

  case class DoublyLinkedListNode[A](var value: A, var prev: Option[DoublyLinkedListNode[A]] = None, var next: Option[DoublyLinkedListNode[A]] = None)

}
