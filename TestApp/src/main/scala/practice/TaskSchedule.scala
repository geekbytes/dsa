package practice

object TaskSchedule extends App {

  //https://leetcode.com/problems/task-scheduler/

  import scala.collection.mutable
  import scala.collection.mutable._

  implicit val ordering: Ordering[Task] = new Ordering[Task] {
    override def compare(x: Task, y: Task): Int = {
      Ordering[Int].compare(x.pending, y.pending)
    }
  }

  val queue = new mutable.PriorityQueue[Task]()

  def leastInterval(tasks: Array[Char], n: Int): Int = {

    val map = Map.empty[Char, Task]

    tasks.foreach(task => {

      map.get(task) match {
        case Some(task) => map.update(task.name, task.copy(pending = task.pending + 1))
        case _          => map.update(task, Task(task))
      }

    })

    map.foreach({
      case (c, task) => queue.enqueue(task)
    })

    val result = ArrayBuffer.empty[Char]

    val stash = new ArrayBuffer[Task]()

    while (queue.nonEmpty) {

      for {
        _ <- 0 to n
      } {

        if (queue.isEmpty) {
          if(stash.nonEmpty) {
            result += '_'
          }
        } else {
          val task = queue.dequeue()
          result += task.name
          if (task.pending > 1) {
            stash += task.copy(pending = task.pending - 1)
          }
        }

      }
      stash.foreach(task => {
        queue.enqueue(task)
      })

      stash.clear()

    }

    println(result.toList.mkString(","))

    result.length

  }

  case class Task(name: Char, var pending: Int = 1)

  println(leastInterval(Array('A', 'A', 'A', 'B', 'B', 'B'), 2))

}
