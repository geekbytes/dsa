package practice

import scala.util.Sorting

object Iternary extends App {

  object Solution {

    def solution(a: Int, b: Int, c: Int): String = {
      // write your code in Scala 2.12

      case class Counter(char: Char, count: Int)

      var result = ""

      def rec(a: Int, b: Int, c: Int, counter: Counter, string: String):Unit = {

        if(string.length > result.length) {
          result = string
        }





        if(a > 0 && counter.char!='a') {
          rec(a-1, b, c,Counter('a', 1), string + 'a')

      }

        if(a > 0 && counter.char=='a' && counter.count < 2) {
          rec(a-1, b, c,Counter('a', 2), string + 'a')

        }

        if(b > 0 && counter.char!='b') {
          rec(a, b-1, c,Counter('b', 1), string + 'b')

        }

        if(b > 0 && counter.char=='b' && counter.count < 2) {
          rec(a, b- 1, c,Counter('b', 2), string + 'b')

        }

        if(c > 0 && counter.char!='c') {
          rec(a, b, c-1,Counter('c', 1), string + 'c')

        }

        if(c > 0 && counter.char=='c' && counter.count < 2) {
          rec(a, b, c -1,Counter('c', 2), string + 'c')

        }




    }

      rec(a,b,c,Counter('!', 0), "")

      result
  }

  import scala.collection.mutable._

  Map.empty[Char, Int] - 'c'

  implicit val ordering: Ordering[FlightDestination] = (x: FlightDestination, y: FlightDestination) =>
    Ordering[String].compare(x.name, y.name)

  def findItinerary(tickets: List[List[String]]): List[String] = {

    val flights = Map.empty[String, Array[FlightDestination]]

    //create graph
    tickets.foreach({
      case from :: to :: _ => {

        val destinations = flights.getOrElse(from, Array.empty[FlightDestination])

        val newDestinations = destinations :+ new FlightDestination(to)

        Sorting.quickSort(newDestinations)

        flights.update(from, newDestinations)

      }
      case _ => ()
    })

    flights.foreach({
      case (from, destinations) => {
        println(s"$from -> ${destinations.map(_.name).mkString(",")}")
      }
    })

    val flightCount = tickets.length

    println(flightCount)

    def takeARoute(origin: String, route: ArrayBuffer[String]): Boolean = {

      if (route.length == flightCount + 1) {
        println(route.length)
        true
      } else {

        flights.get(origin) match {
          case Some(destinations) => {

            var i = 0

            while (i < destinations.length) {
              val nextFlight = destinations(i)
              if (!nextFlight.taken) {
                nextFlight.taken = true
                route.append(nextFlight.name)
                val isFeasible = takeARoute(nextFlight.name, route)

                if (isFeasible) {
                  return true
                } else {
                  route.remove(route.length - 1, 1)
                  nextFlight.taken = false
                  i = i + 1
                }
              } else {
                i = i + 1
              }
            }

            i != destinations.length

          }
          case _ => {
            false
          }
        }

      }

    }

    val route = ArrayBuffer[String]("JFK")

    takeARoute("JFK", route)

    route.toList

  }

  class FlightDestination(val name: String, var taken: Boolean = false)

  println(findItinerary(List(List("JFK", "KUL"), List("JFK", "NRT"), List("NRT", "JFK"))))

}
