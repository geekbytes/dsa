package practice



object kClosestPoint extends App {

  //https://leetcode.com/problems/merge-k-sorted-lists/solution/

  import java.util.concurrent.ThreadLocalRandom

  import scala.annotation.tailrec
  import scala.util.control.Breaks._

  def kClosest(points: Array[Array[Int]], K: Int): Array[Array[Int]] = {

    def partition(l: Int, h: Int) = {

      val pivot = dist(l)

      var i = l + 1
      var j = h

      breakable {
        while (true) {

          while (i < j && dist(i) < pivot) {
            i += 1
          }

          while (i <= j && dist(j) > pivot) {
            j = j - 1
          }

          if (i >= j) break

          swap(points, i, j)

        }
      }

      swap(points, l, j)

      j

    }

    @tailrec
    def sort(l: Int, h: Int, K: Int): Unit = {
      if (l < h) {

        val k = ThreadLocalRandom.current().nextInt(l, h)
        swap(points, l, k)
        val p = partition(l, h)

        val left = p - l + 1

        if (K < left) {
          sort(l, p - 1, K)
        } else if (K > left) {
          sort(p + 1, h, K - left)
        }
      }

    }

    def dist(i: Int) = points(i)(0) * points(i)(0) + points(i)(1) * points(i)(1)

    def swap[A](array: Array[A], i: Int, j: Int): Unit = {

      val temp = array(i)
      array.update(i, array(j))
      array.update(j, temp)

    }

    sort(0, points.length - 1, K)

    points.take(K)

  }

}
