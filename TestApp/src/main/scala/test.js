function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)

    let n = A.length;

    function swap(i) {
        let temp=A[A[i]-1];
        A[A[i]-1]=A[i];
        A[i]=temp;
    }

    for(let i = 0; i < n; i++)
    {
        while (A[i] >= 1 && A[i] <= n && A[i] !== A[A[i] - 1]) {
            swap(i);
        }
    }

    for(let i = 0; i < n; i++)
        if (A[i] !== i + 1)
            return (i + 1);

    // for values 1 to n
    return (n + 1);
}